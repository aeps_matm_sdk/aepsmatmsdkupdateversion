package com.matm.matmsdk.aepsmodule.utils;


import android.content.Context;
import android.content.SharedPreferences;


public class Session {
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context ctx;


    public String getUserLatLong() {
        return prefs.getString(AepsSdkConstants.USER_LATLONG, null);
    }

    public void setUserLatLong(String userToken) {
        editor.putString(AepsSdkConstants.USER_LATLONG, userToken);
        editor.commit();
    }

    public String getUserPincode() {
        return prefs.getString(AepsSdkConstants.USER_PINCODE, null);
    }

    public void setUserPincode(String userToken) {
        editor.putString(AepsSdkConstants.USER_PINCODE, userToken);
        editor.commit();
    }

    public String getUserCity() {
        return prefs.getString(AepsSdkConstants.USER_CITY, null);
    }

    public void setUserCity(String userToken) {
        editor.putString(AepsSdkConstants.USER_CITY, userToken);
        editor.commit();
    }

    public String getUserState() {
        return prefs.getString(AepsSdkConstants.USER_STATE, null);
    }

    public void setUserState(String userToken) {
        editor.putString(AepsSdkConstants.USER_STATE, userToken);
        editor.commit();
    }


    public String getUserToken() {
        return prefs.getString(AepsSdkConstants.USER_TOKEN_KEY, null);
    }

    public void setUserToken(String userToken) {
        editor.putString(AepsSdkConstants.USER_TOKEN_KEY, userToken);
        editor.commit();
    }

    public String getFreshnessFactor() {
        return prefs.getString(AepsSdkConstants.NEXT_FRESHNESS_FACTOR, null);
    }

    public void setFreshnessFactor(String freshnessFactor) {
        editor.putString(AepsSdkConstants.NEXT_FRESHNESS_FACTOR, freshnessFactor);
        editor.commit();
    }



    public void clear(){
        prefs = ctx.getSharedPreferences(AepsSdkConstants.EASY_AEPS_PREF_KEY, Context.MODE_PRIVATE);
        editor = prefs.edit();
        editor.clear().commit();
    }


    public Session(Context ctx) {
        this.ctx = ctx;
        prefs = ctx.getSharedPreferences(AepsSdkConstants.EASY_AEPS_PREF_KEY, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void setLoggedIn(boolean logggedIn) {
        editor.putBoolean(AepsSdkConstants.EASY_AEPS_USER_LOGGED_IN_KEY, logggedIn);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return prefs.getBoolean(AepsSdkConstants.EASY_AEPS_USER_LOGGED_IN_KEY, false);
    }
    public void setUsername(String username) {
        editor.putString (AepsSdkConstants.EASY_AEPS_USER_NAME_KEY, username);
        editor.commit();
    }

    public String getUserName() {
        return prefs.getString (AepsSdkConstants.EASY_AEPS_USER_NAME_KEY, null);
    }
    public void setEncryptedString(String encryptedString) {
        editor.putString (AepsSdkConstants.encryptedString, encryptedString);
        editor.commit();
    }


    public String getEncryptedString() {
        return prefs.getString(AepsSdkConstants.encryptedString, null);
    }
}