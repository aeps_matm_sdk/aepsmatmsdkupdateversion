package com.pax.pax_sdk_app;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.google.firebase.FirebaseApp;
import com.matm.matmsdk.Bluetooth.BluetoothActivity;
import com.matm.matmsdk.DemoActivity;
import com.matm.matmsdk.MPOS.PosActivity;
import com.matm.matmsdk.Utils.MATMSDKConstant;
import com.matm.matmsdk.aepsmodule.AEPS2HomeActivity;
import com.matm.matmsdk.aepsmodule.AEPSHomeActivity;
import com.matm.matmsdk.aepsmodule.CoreAEPSHomeActivity;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    RadioGroup rgTransactionType;
    RadioButton rbCashWithdrawal;
    RadioButton rbBalanceEnquiry,rb_mini;
    EditText etAmount,et_paramA,et_paramB;
    Button btnProceed,btn_pair,btn_proceedaeps,upiPagebtn,BtnUnpair,Btndriver;
    public static final int REQUEST_CODE = 5;
    ProgressDialog pd;
    Boolean isAepsClicked = false,isMatmClicked = false;
    String manufactureFlag = "";
    UsbManager musbManager;
    private UsbDevice usbDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        FirebaseApp.initializeApp(MainActivity.this);

        //retriveUserList();
        String str = "609384625620";
        String hashString = getSha256Hash(str);
        System.out.println("String Value :"+hashString);
    }



    private void initView() {
        musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
        rgTransactionType = findViewById(R.id.rg_trans_type);
        rbCashWithdrawal = findViewById(R.id.rb_cw);
        rbBalanceEnquiry = findViewById(R.id.rb_be);
        btnProceed = findViewById(R.id.btn_proceed);
        btn_pair = findViewById(R.id.btn_pair);
        rb_mini = findViewById(R.id.rb_mini);
        etAmount = findViewById(R.id.et_amount);
        btn_proceedaeps = findViewById(R.id.proceedBtn);
        upiPagebtn =findViewById(R.id.upiPagebtn);
        BtnUnpair = findViewById(R.id.BtnUnpair);
        Btndriver = findViewById(R.id.Btndriver);

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MATMSDKConstant.BlueToothPairFlag.equalsIgnoreCase("1")){
                    if(PosActivity.isBlueToothConnected(MainActivity.this)){
                        callMATMSDKApp();
                    }else {
                        Toast.makeText(MainActivity.this, "Please pair the bluetooth device", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(MainActivity.this,"Please pair bluetooth device for ATM transaction.",Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_proceedaeps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(biometricDeviceConnect()){
                    callAEPSSDKApp();
                }else{
                    Toast.makeText(MainActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
                intent.putExtra("userName","itpl");
                intent.putExtra("user_token","eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTk0MDI2NjkzMTkwLCJleHAiOjE1OTQwMjg0OTN9.u6Ac5npPCrONQCKUN7qLU_pAb0upmNT4Epw8h-ssvVP9YFeiIsZKtBDFJu87VP-tkisZVAcK2yuAfURMOtGU9Q");

                MATMSDKConstant.applicationType ="CORE";
                MATMSDKConstant.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTk0MDI2NjkzMTkwLCJleHAiOjE1OTQwMjg0OTN9.u6Ac5npPCrONQCKUN7qLU_pAb0upmNT4Epw8h-ssvVP9YFeiIsZKtBDFJu87VP-tkisZVAcK2yuAfURMOtGU9Q";
                MATMSDKConstant.userNameFromCoreApp = "itpl";


               // MATMSDKConstant.loginID = "8895274718";
                //MATMSDKConstant.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVpG%2BMXYkFjv7FvFq5zSop426IBfOKVTFtcsgZDUCORAu%2FDJvr85SGAUeQVWgRINTI5teZqYzUL1nyFMcf1eO69A%3D";


                // MATMSDKConstant.encryptedData ="TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";


               // MATMSDKConstant.loginID = "aepsTestR";
                //MATMSDKConstant.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";
                //MATMSDKConstant.applicationType ="CORE";
                //MATMSDKConstant.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTkwNzU0MDk3ODEyLCJleHAiOjE1OTA3NTU4OTd9.KZzIVVH9Snh9ZUUaxH_q8MrTleFgq-RQDFuj0Ep1K_VIvM2SZH0-quBg-Z9ixzubcswucWV6L8UA2_TWS2xs7w";

               // MATMSDKConstant.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";
                //MATMSDKConstant.applicationType ="CORE";
                //MATMSDKConstant.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTkzNDExNTk4MzU0LCJleHAiOjE1OTM0MTMzOTh9.lJ12V8CH9QjyNkFbxHIqcJ4z3LJo4VpmTEFKa5EW1C5tuOd-uCieq7gYXWLW3kZ9mwj54M3Kf6LsamleGx0bEg";
                //MATMSDKConstant.userNameFromCoreApp = "demoisu";

                //MATMSDKConstant.loginID = "aepsTestR";
                //MATMSDKConstant.encryptedData ="TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH,
                                    Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(),"Please Grant all the permissions", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(intent);
                        }
                    }else {
                        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH,
                                    Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(intent);
                        }
                    }
                }
                    else{
                    startActivity(intent);
                }
            }
        });
        BtnUnpair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MATMSDKConstant.LogOut = "0";
                MATMSDKConstant.BlueToothPairFlag = "0";
                Intent intent = new Intent(MainActivity.this,BluetoothActivity.class);
                intent.putExtra("user_id","488");
                intent.putExtra("user_token","eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTkzMjcwOTQzNzcwLCJleHAiOjE1OTMyNzI3NDN9.U6OHRnz-UvkcDOicmUYm_yT8FSDofbzZ9H8kJsLdLwgTRwt5vJ0PIwTKYC1JRpM3kXwqGg6sCpvQn1PmUz9lgw");

                startActivity(intent);
            }
        });

        Btndriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DemoActivity.class);
                intent.putExtra("activity","com.pax.pax_sdk_app.DriverActivity");
                intent.putExtra("driverFlag","MANTRA");

                startActivity(intent);
            }
        });

        upiPagebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UPIMainActivity.class);
                startActivity(intent);
            }
        });

        rgTransactionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_cw) {
                    etAmount.setClickable(true);
                    etAmount.setHint("Amount");
                    etAmount.setVisibility(View.VISIBLE);
                    etAmount.setText("");
                    etAmount.setEnabled(true);
                    etAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
                else if (checkedId == R.id.rb_be) {
                    etAmount.setVisibility(View.GONE);
                    etAmount.setClickable(false);
                    etAmount.setEnabled(false);
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null & resultCode == RESULT_OK) {

            /**
             *  FOR AEPS TRANSACTION RESPONSE
             */
            if (requestCode == MATMSDKConstant.REQUEST_CODE) {
                String response = data.getStringExtra(MATMSDKConstant.responseData);
                System.out.println("Response: "+response);
                //Toast.makeText(MainActivity.this,response,Toast.LENGTH_SHORT).show();
            }
            /**
             *  FOR MATM TRANSACTION RESPONSE
             */
            if (requestCode == MATMSDKConstant.REQUEST_CODE) {
                String response = data.getStringExtra(MATMSDKConstant.responseData);
                System.out.println("Response: "+response);
                //Toast.makeText(MainActivity.this,response,Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void callAEPSSDKApp() {
        AepsSdkConstants.transactionAmount = etAmount.getText().toString().trim();
        if(rbCashWithdrawal.isChecked()){
            AepsSdkConstants.transactionType = AepsSdkConstants.cashWithdrawal;

        }
        else if(rbBalanceEnquiry.isChecked()){
            AepsSdkConstants.transactionType= AepsSdkConstants.balanceEnquiry;
        }
        else if(rb_mini.isChecked()){
            AepsSdkConstants.transactionType= AepsSdkConstants.ministatement;
        }

        AepsSdkConstants.paramA = "annapurna";
        AepsSdkConstants.paramB = "BLS1";
        AepsSdkConstants.paramC = "loanID";
        AepsSdkConstants.loginID = "aepsTestR";
        AepsSdkConstants.encryptedData ="TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";
        AepsSdkConstants.applicationType ="CORE";
        AepsSdkConstants.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTk0NjQ5MjIxMzg5LCJleHAiOjE1OTQ2NTEwMjF9.zbH7KM6Z9gZBjqGL3jnHZzHQL5ISDNzNHMWUnrzsGLq179svqT_udmT1D9xPmLTlemrLMj7a4y2flisq7GCeBw";
        AepsSdkConstants.userNameFromCoreApp ="demoisu";

        //AepsSdkConstants.bioauth =true;
        //AepsSdkConstants.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqno3cxOh6wUl5r4YDSECZYVg%3D%3D";
       // FOR AEPS2
       // AepsSdkConstants.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";
      // INSTA MUDRA
      //  AepsSdkConstants.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";


        System.out.println("Rajesh::::"+manufactureFlag);

        Intent intent = new Intent(MainActivity.this, CoreAEPSHomeActivity.class);
        intent.putExtra("activity","com.pax.pax_sdk_app.DriverActivity");
        intent.putExtra("driverFlag",manufactureFlag);
        startActivity(intent);


    }

    private void callMATMSDKApp() {
        MATMSDKConstant.transactionAmount = etAmount.getText().toString().trim();
        if(rbCashWithdrawal.isChecked()){
            MATMSDKConstant.transactionType = MATMSDKConstant.cashWithdrawal;
            MATMSDKConstant.transactionAmount = etAmount.getText().toString();

        }if(rbBalanceEnquiry.isChecked()){
            MATMSDKConstant.transactionType= MATMSDKConstant.balanceEnquiry;
            MATMSDKConstant.transactionAmount = "0";
        }
        MATMSDKConstant.paramA = "123456789";
        MATMSDKConstant.paramB = "branch1";
        MATMSDKConstant.paramC = "loanID1234";
        //MATMSDKConstant.loginID = "aepsTestR";

        MATMSDKConstant.applicationType ="CORE";
        MATMSDKConstant.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTk0MDI2NjkzMTkwLCJleHAiOjE1OTQwMjg0OTN9.u6Ac5npPCrONQCKUN7qLU_pAb0upmNT4Epw8h-ssvVP9YFeiIsZKtBDFJu87VP-tkisZVAcK2yuAfURMOtGU9Q";
        MATMSDKConstant.userNameFromCoreApp = "itpl";



        //MATMSDKConstant.loginID = "8895274718";
        //MATMSDKConstant.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVpG%2BMXYkFjv7FvFq5zSop426IBfOKVTFtcsgZDUCORAu%2FDJvr85SGAUeQVWgRINTI5teZqYzUL1nyFMcf1eO69A%3D";


        // MATMSDKConstant.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTkwNzU0MDk3ODEyLCJleHAiOjE1OTA3NTU4OTd9.KZzIVVH9Snh9ZUUaxH_q8MrTleFgq-RQDFuj0Ep1K_VIvM2SZH0-quBg-Z9ixzubcswucWV6L8UA2_TWS2xs7w";

       // MATMSDKConstant.encryptedData = "cssC%2BcHGxugRFLTjpk%2BJN2Hbbo%2F%2BDokPsBwb9uFdXebdGg%2FEaqOvFXBEoU7ve%2FAPTvmZ48yn4JCqcQ5Z6H09U%2B8x0I80PAnJMXPAFqayZ2Ojh6mRYKWc7D2YuCQC9hAR";
       // MATMSDKConstant.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";
        //MATMSDKConstant.encryptedData ="TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqnJco8zzlZdBiKwXAJzsBr8g%3D%3D";


        Intent intent = new Intent(MainActivity.this, PosActivity.class);
        startActivityForResult(intent, MATMSDKConstant.REQUEST_CODE);
    }

    @Override
    protected void onResume() {
        String str = MATMSDKConstant.responseData;
        Toast.makeText(getApplicationContext(),str,Toast.LENGTH_LONG).show();
        super.onResume();
    }


    public  String getSha256Hash(String password) {
        try {
            MessageDigest digest = null;
            try {
                digest = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e1) {
                e1.printStackTrace();
            }
            digest.reset();
            return bin2hex(digest.digest(password.getBytes()));
        } catch (Exception ignored) {
            return null;
        }
    }

    private  String bin2hex(byte[] data) {
        StringBuilder hex = new StringBuilder(data.length * 2);
        for (byte b : data)
            hex.append(String.format("%02x", b & 0xFF));
        return hex.toString();
    }

    private Boolean biometricDeviceConnect(){
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae();
            return false;
        }
        else {
            for (UsbDevice device : connectedDevices.values()) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(device !=null && device.getManufacturerName () != null){
                        usbDevice = device;
                        manufactureFlag = usbDevice.getManufacturerName();
                        return true;
                    }

                }
            }
        }
        return false;
    }

    private void deviceConnectMessgae (){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle("No device found")
                .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                       // finish();
                    }
                })
                .show();
    }
}
